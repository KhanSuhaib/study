package gradle.util;

import org.junit.Test;
import static org.junit.Assert.*;

import org.junit.Before;

public class MathUtilsTest {
	
	private MathUtils test;
	
	@Before
	public void init() {
		test = new MathUtils();
	}
	
	@Test
	public void addTest() {
		
		assertEquals(test.add(3, 4), 7);
		assertEquals(test.add(0, 5), 5);
		assertEquals(test.add(8, 0), 8);
		assertEquals(test.add(8, -5), 3);
		assertEquals(test.add(-19, 2), -17);
		
	}
}
